# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str_array = str.split
  fin_hash={}
  str_array.each do |string|
    fin_hash[string]=string.length
  end
  fin_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  largest=0
  fin_key=nil
  hash.each do |k,v|
    if v > largest
      largest = v
      fin_key=k
    end
  end
  fin_key
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k]=v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_count=Hash.new(0)
  word.chars.each do |char|
    word_count[char]+=1
  end
  word_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  new_hash={}
  arr.each do |value|
    new_hash[value]=true
  end
  new_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  frequencies=Hash.new(0)
  numbers.each do |number|
    if number % 2 == 0
      frequencies[:even]+=1
    else
      frequencies[:odd]+=1
    end
  end
  frequencies
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels="aieou"
  frequency=Hash.new(0)
  string.chars do |char|
    if vowels.include? char
      frequency[char]+=1
    end
  end
  max=frequency.values.max
  vowels.chars.each do |vowel|
    if frequency[vowel] == max
      return vowel
    end
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fin_array=[]
  array=students.to_a
  array=array.select {|k,v| v > 6}.map {|key,v| key}
  array.each_with_index do |person,idx|
    while idx < array.length-1
      fin_array.push([person,array[idx+1]])
      idx +=1
    end
  end
  fin_array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  count_specimens=Hash.new(0)
  specimens.each do |specimen|
    count_specimens[specimen]+=1
  end
  number_of_species = count_specimens.length
  smallest_pop=count_specimens.sort_by(&:last)[0][1]
  largest_pop=count_specimens.sort_by(&:last)[-1][1]
  number_of_species ** 2 * smallest_pop / largest_pop
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_count=Hash.new(0)
  normal_sign.downcase.chars.each do |char|
    normal_sign_count[char] += 1
  end
  vandalized_sign_count=Hash.new(0)
  vandalized_sign.downcase.chars.each do |char|
    vandalized_sign_count[char] += 1
  end
  vandalized_sign_count.keys.each do |key|
    if normal_sign_count[key]-vandalized_sign_count[key] < 0
      return false
    end
  end
  true
end

def character_count(str)
end
